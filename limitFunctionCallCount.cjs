function limitFunctionCallCount(cb, n) {

    if (arguments.length !== 2 || typeof(cb) !== 'function' || typeof(n) !== 'number') {
        throw new Error('The callback function or the number is undefined or not of the same type');
    }

    let callCount = 0;

    return function (...args) {
        if (callCount < n) {
            callCount++;
            return cb(...args);
        }
        return null;
    };

}

module.exports = limitFunctionCallCount;