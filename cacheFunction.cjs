function cacheFunction(cb) {
    
    if (arguments.length !== 1 || typeof(cb) !== 'function') {
        throw new Error('The callback function is undefined or incomplete');
    } 
    
    const cache = {};

    return function (...args) {
        const key = JSON.stringify(args);
        if (cache.hasOwnProperty(key)) {
            return cache[key];
        }
        const result = cb(...args);
        cache[key] = result;
        return result;
    };
}

module.exports = cacheFunction;