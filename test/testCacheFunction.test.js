const cacheFunction = require('../cacheFunction.cjs');

let callCount = 0;

const addWithCallCount = (a, b) => {
    callCount++;
    return a + b;
}
const cacheAdd = cacheFunction(addWithCallCount);

test('Testing cacheFunction', () => {
    expect(cacheAdd(1, 2)).toStrictEqual(3);
    expect(callCount).toStrictEqual(1);
    expect(cacheAdd(1, 2)).toStrictEqual(3);
    expect(callCount).toStrictEqual(1);
    expect(cacheAdd(2, 3)).toStrictEqual(5);
    expect(callCount).toStrictEqual(2);
});

try {
    const cacheAdd2 = cacheFunction();
    console.log(cacheAdd2(1, 2));

} catch (e) {
    console.error(e);
}
