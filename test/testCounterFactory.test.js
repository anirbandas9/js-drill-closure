const counterFactory = require('../counterFactory.cjs');

const counter = counterFactory();

test('Testing counterFactory function', () => {
    expect(counter.increment()).toStrictEqual(1);
    expect(counter.increment()).toStrictEqual(2);
    expect(counter.decrement()).toStrictEqual(1);
});