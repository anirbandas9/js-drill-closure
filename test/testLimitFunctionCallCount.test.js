const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

const limitedAdd = limitFunctionCallCount((a, b) => a + b, 2);

test('Testing limitFunctionCallCount function', () => {
    expect(limitedAdd(1, 2)).toStrictEqual(3);
    expect(limitedAdd(1, 2)).toStrictEqual(3);
    expect(limitedAdd(1, 2)).toStrictEqual(null);
});

try {
    const limitedAdd2 = limitFunctionCallCount((a, b) => a + b);
    console.log(limitedAdd2(1, 2));

} catch (e) {
    console.error(e);
}

try {
    const limitedAdd3 = limitFunctionCallCount();
    console.log(limitedAdd3(1, 2));
    
} catch (e) {
    console.error(e);
}

